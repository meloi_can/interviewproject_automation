let resolutions = [[1920, 1200, 'Desktop']];

let dropdownOptions_Categories = [
  'All',
  'Computers',
  'Computers >> Desktops',
  'Computers >> Notebooks',
  'Computers >> Software',
  'Electronics',
  'Electronics >> Camera & photo',
  'Electronics >> Cell phones',
  'Electronics >> Others',
  'Apparel',
  'Apparel >> Shoes',
  'Apparel >> Clothing',
  'Apparel >> Accessories',
  'Digital downloads',
  'Books',
  'Gift Cards',
  'Jewelry'
];

let dropdownOptions_Categories_alphabetical = dropdownOptions_Categories.sort();

let dropdownOptions_ResultsPerPage = ['10', '20', '50', '100'];

let dropdownOptions_SortBy = ['Position', 'Name: A to Z', 'Name: Z to A', 'Price: Low to High', 'Price: High to Low', 'Created on'];

let dropdownOptions_Manufacturer = ['Apple', 'Lenovo', 'Nokia', 'Our brand'];

describe(`InterviewProject - Search engine - Test suite`, () => {
  resolutions.forEach((resolution) => {
    describe(resolution[2] + ` suite`, () => {
      before(function () {
        cy.viewport(resolution[0], resolution[1]);
      });

      describe(`User reaches main page`, () => {
        context(`Actions`, () => {
          it(`Visiting the main page`, () => {
            cy.visit('demo.nopcommerce.com');
          });
        });

        context(`Assertions`, () => {
          it(`Search input is visible and contains "Search item"`, () => {
            cy.get('.header .search-box-text').should('be.visible').contains('Search item');
          });

          it(`Search button is visible and contains "Search"`, () => {
            cy.get('.header .search-box-button').should('be.visible').contains('Search');
          });
        });
      });

      describe(`User enters a valid keyword which yields multiple results and presses the "Search" button`, () => {
        context(`Actions`, () => {
          it(`Entering the keyword "pro" into the search input`, () => {
            cy.get('.header .search-box-text').type('pro');
          });

          it(`Pressing the "Search" button`, () => {
            cy.get('.header .search-box-button').click();
          });
        });

        context(`Assertions`, () => {
          it(`Title of the page is visible and contains "Search the Store"`, () => {
            cy.get('.page-title h1').should('be.visible').contains('Search the Store');
          });

          it(`Advanced search options are not visible`, () => {
            cy.get('.advanced-search').should('not.be.visible');
          });

          it(`A maximum of 3 results per row are displayed`, () => {
            cy.get('.item-grid')
              .invoke('width')
              .then((width) => {
                expect(width).to.be.at.most(876);
              });
          });

          it(`Each result has a product name`, () => {
            cy.get('.item-box .product-title').each((productTitle) => {
              cy.wrap(productTitle).should('be.visible').should('not.be.empty');
            });
          });

          it(`Each result has an image`, () => {
            cy.get('.item-box .picture img').each((productImage) => {
              cy.wrap(productImage).should('be.visible').should('have.attr', 'src').should('not.be.empty');
            });
          });

          it(`Each result has a link`, () => {
            cy.get('.item-box .picture a').each((productLink) => {
              cy.wrap(productLink).should('have.attr', 'href').should('not.be.empty');
            });
          });

          it(`Each result has a rating`, () => {
            cy.get('.item-box .product-rating-box .rating').each((productRating) => {
              cy.wrap(productRating).should('be.visible').should('not.be.empty');
            });
          });

          it(`Each result has a price`, () => {
            cy.get('.item-box .prices .actual-price').each((productPrice) => {
              cy.wrap(productPrice).should('be.visible').should('contain', '$');
            });
          });

          it(`Each result has a "Add to cart" button`, () => {
            cy.get('.item-box .product-box-add-to-cart-button').each((productAddCart_button) => {
              cy.wrap(productAddCart_button).should('be.visible').should('have.attr', 'value', 'Add to cart');
            });
          });

          it(`Each result has a comparison button visible`, () => {
            cy.get('.item-box .product-box-add-to-cart-button').each((productCompare_button) => {
              cy.wrap(productCompare_button).should('be.visible');
            });
          });

          it(`Each result has a wishlist button visible`, () => {
            cy.get('.item-box .add-to-wishlist-button').each((productWishlist_button) => {
              cy.wrap(productWishlist_button).should('be.visible');
            });
          });

          it(`"Sort by" dropdown menu and label are visible`, () => {
            cy.get('.product-selectors #products-orderby').should('be.visible');
            cy.get('.product-sorting span').should('be.visible').contains('Sort by');
          });

          it(`"Position" is selected by default in the "Sort by" menu`, () => {
            cy.get('.product-selectors #products-orderby').contains('Position');
          });

          it(`"Show _ results per page" label and dropdown are visible.`, () => {
            cy.get('.product-selectors .product-page-size #products-pagesize').should('be.visible');
            cy.get('.product-selectors .product-page-size span').eq(0).should('be.visible').contains('Show');
            cy.get('.product-selectors .product-page-size span').eq(1).should('be.visible').contains('results per page');
          });

          it(`No option is selected by default in the "Show _ results per page" menu`, () => {
            cy.get('.product-selectors .product-page-size #products-pagesize').should('be.empty');
          });

          it(`"Page 1 of [number]" is visible`, () => {
            cy.get('.numbers_of_pages_string').should('contain', 'Page 1 of'); //This is missing
          });
        });
      });

      describe(`User selects the "Advanced search" checkbox`, () => {
        context(`Actions`, () => {
          it(`Checking the box for "Advanced search"`, () => {
            cy.get('.basic-search #adv').click();
          });
        });

        context(`Assertions`, () => {
          it(`Category dropdown and label are visible`, () => {
            cy.get('.advanced-search .inputs:nth-child(1) label').should('be.visible').should('contain', 'Category');
            cy.get('.advanced-search .inputs:nth-child(1) #cid').should('be.visible');
          });

          it(`Category "All" is selected by default`, () => {
            cy.get('.advanced-search #cid').contains('All');
          });

          it(`"Automatically search sub categories" checkbox and label are visible`, () => {
            cy.get('#advanced-search-block .inputs label').should('be.visible').contains('Automatically search sub categories');
            cy.get('#advanced-search-block .inputs #isc').should('be.visible');
          });

          it(`"Automatically search sub categories" checkbox can be checked`, () => {
            cy.get('#advanced-search-block .inputs #isc').click();
            cy.get('#advanced-search-block .inputs #isc').check('true');
            cy.get('#advanced-search-block .inputs #isc').click();
          });

          it(`"Search in product descriptions" checkbox and label are visible`, () => {
            cy.get('#advanced-search-block .inputs label').should('be.visible').contains('Search In product descriptions');
            cy.get('#advanced-search-block .inputs #sid').should('be.visible');
          });

          it(`"Search in product descriptions" checkbox can be checked`, () => {
            cy.get('#advanced-search-block .inputs #sid').click();
            cy.get('#advanced-search-block .inputs #sid').check('true');
            cy.get('#advanced-search-block .inputs #sid').click();
          });

          it(`"Manufacturer" dropdown and label are visible`, () => {
            cy.get('.advanced-search .inputs:nth-child(3) label').should('be.visible').should('contain', 'Manufacturer');
            cy.get('.advanced-search .inputs:nth-child(3) #mid').should('be.visible');
          });

          it(`Manufacturer "All" is selected by default`, () => {
            cy.get('.advanced-search .inputs:nth-child(3) #mid').contains('All');
          });

          it(`"Price range" inputs are visible`, () => {
            cy.get('.advanced-search .inputs:nth-child(4) #pf').should('be.visible');
            cy.get('.advanced-search .inputs:nth-child(4) #pt').should('be.visible');
          });

          it(`"Price range", "From:" and "To:" labels are visible`, () => {
            cy.get('.advanced-search .inputs:nth-child(4) label').should('be.visible').should('contain', 'Price range');
            cy.get('.advanced-search .inputs:nth-child(4) label').should('be.visible').should('contain', 'From:'); //This is missing
            cy.get('.advanced-search .inputs:nth-child(4) label').should('be.visible').should('contain', 'To:'); //This is missing
          });
        });
      });

      describe(`User selects "Category" dropdown menu which contains a specific list of values in alphabetical order`, () => {
        context(`Assertions`, () => {
          it(`Category options has a correct list of values in alphabetical order`, () => {
            cy.get('.advanced-search #cid option').should(($options) => {
              const optionsValues = $options.toArray().map((element) => element.innerText);
              expect(optionsValues).to.deep.eq(dropdownOptions_Categories_alphabetical);
            });
          });
        });
      });

      describe(`User selects "Manufacturer" dropdown menu which contains a specific list of options`, () => {
        context(`Assertions`, () => {
          it(`Manufacturer menu has a correct list of options`, () => {
            cy.get('.advanced-search #mid option').should(($options) => {
              const optionsValues = $options.toArray().map((element) => element.innerText);
              expect(optionsValues).to.deep.eq(dropdownOptions_Manufacturer);
            });
          });
        });
      });

      describe(`User selects "Sort by" dropdown menu which contains a specific list of options`, () => {
        context(`Assertions`, () => {
          it(`"Sort by"  menu has a correct list of options`, () => {
            cy.get('.product-selectors #products-orderby option').should(($options) => {
              const optionsValues = $options.toArray().map((element) => element.innerText);
              expect(optionsValues).to.deep.eq(dropdownOptions_SortBy);
            });
          });
        });
      });

      describe(`User selects "Show _ results per page" dropdown menu which contains a specific list of options`, () => {
        context(`Assertions`, () => {
          it(`"Show _ results per page" menu has a correct list of options`, () => {
            cy.get('.product-selectors #products-pagesize option').should(($options) => {
              const optionsValues = $options.toArray().map((element) => element.innerText);
              expect(optionsValues).to.deep.eq(dropdownOptions_ResultsPerPage);
            });
          });
        });
      });

      describe(`User clicks on the button to view results as list`, () => {
        context(`Actions`, () => {
          it(`Selecting the button has no effect`, () => {
            cy.get('.viewmode-icon.list').should('not.have.attr', 'href');
          });
        });
      });

      describe(`User types in a single valid letter into the search box and presses the "Search" button`, () => {
        context(`Actions`, () => {
          it(`Typing "a" into the search input`, () => {
            cy.get('.search-input .inputs .search-text').clear().type('a');
          });

          it(`Pressing the "Search" button`, () => {
            cy.get('.search-input .buttons .search-button').click();
          });
        });

        context(`Assertions`, () => {
          it(`At least one result is displayed`, () => {
            cy.get('.search-results .product-grid .item-box').should('be.visible');
          });
        });
      });

      describe(`User types in an invalid keyword into the search box and presses the "Search" button`, () => {
        context(`Actions`, () => {
          it(`Typing "test12345" into the search input`, () => {
            cy.get('.search-input .inputs .search-text').clear().type('test12345');
          });

          it(`Pressing the "Search" button`, () => {
            cy.get('.search-input .buttons .search-button').click();
          });
        });

        context(`Assertions`, () => {
          it(`There are no results displayed`, () => {
            cy.get('.search-results .product-grid .item-box').should('not.be.visible');
          });

          it(`The string "No results for test1245." should be visible`, () => {
            cy.get('.search-results .no-result').should('be.visible').contains('No results for test1245.');
          });

          it(`The string "No results for test1245." should be blue`, () => {
            cy.get('.search-results .no-result').should('have.css', 'color', 'blue');
          });
        });
      });
    });
  });
});
