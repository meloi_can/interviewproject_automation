# README

This is a simple test project for a fictive company which needs automation tests on their search engine.

## Installation of npm to run the commands

```
https://www.npmjs.com/get-npm
```

## Commands

### Installation of all the dependencies required for running the automation tests

```
npm install
```

### Running the "Search engine" test suite in headless mode in the console

-   A report will be created at the end of the test run into cypress/reports/.
-   The previous test report will be overwritten

```
test:console
```

### Opening Cypress' GUI for running tests in headed mode

```
test:gui
```
